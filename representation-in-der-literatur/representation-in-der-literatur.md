---
title: Representation in der Literatur
---

# Representation in der Literatur - Anhand einer Umfrage zum Gay Romance Genre

## Was ist Representation?
* eine realistische, detailgetreue Darstellung oder auch Abbildung der Realitaet oder auch der Weltgesellschaft
* if the world was a village...
	* 30 percent non-black, 70 percent black
	* 10 queer, 90 hetero
	* Intersectionality yay
* Wieso gibt es keine Vielfalt in Buechern wenn es das in der Gesellschaft gibt?
## Wozu brauche ich Representation?
	* y not escapism?
	* Selbstbild: Dinge, die ich ueber mich erzaehle
	* Zum Beispiel: Selbstwert -> z.B. via Knowledge, representation projection on Hermione
	* Identifikationspunkte
		* the upside of unrequited (Albertalli) as an example for body positivity
		* underrepresented & marginalized people aren't mostly represented in books
## Warum brauchen wir Representation?
	* there's more than white-cis-het people
	* reading helps shaping an understanding
	* broaden horizons

## Ist jede Representation eine gute Representation?
	* according to their survey:
		* tn. totally nope.
		* problematic character tropes altering perception of reality.

## Tropes
	* gay best friend
		* cliches as well dressed gay males, tomboyish lesbians and so on
	* Heteronormative Beziehungsideale werden in Stories reproduziert, sometimes toxic af
	* gay men always crave for sex
		* that's wrong and say it loud: ace gay men exist
	* Seme/Uke - D/s dynamics - Dub-Con (it's called rape, without safewords)
	* gay romance for woman
		* personal annotation of ann: it's mentioned in "a lesbian guide to loneliness" at some point
	* systemic sexualisation
		* discrimination based upon those tropes

## representation of sexuality
	* there's a spectrum, bi, pan and other sexualities are part of it
	* suddenly gay trope
	* "you have to decide" bi-erasure
	* we should talk about ace-erasure as well!
	* when do we talk about polyamory?

## own voices
	* of course, you can write about everything you want, but you just have to reflect, that:
		* you probably won't ever be able to relate to experiences of marginalisation
		* you should reflect what place you have in your genre, since
			* a large percentage of gay fiction isn't written by gay authors

## what you can do in terms of writing
	* sensitive readers
	* personal relationships
	* do your research! please!
	* check your expectations
	* accept criticism by marginalised groups, they probably know stuff better than you do.
